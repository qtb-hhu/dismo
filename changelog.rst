Changelog
=========


1.0.58 (2024-12-29)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.57' into 'main' [qtbadmin qtbadmin]

  1.0.57

  See merge request qtb-hhu/dismo!58
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.57 (2024-12-22)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.56' into 'main' [qtbadmin qtbadmin]

  1.0.56

  See merge request qtb-hhu/dismo!57
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.56 (2024-12-15)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.55' into 'main' [qtbadmin qtbadmin]

  1.0.55

  See merge request qtb-hhu/dismo!56
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.55 (2024-12-08)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.54' into 'main' [qtbadmin qtbadmin]

  1.0.54

  See merge request qtb-hhu/dismo!55
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.54 (2024-12-01)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Bot: chg: updated pre-commit. [daniel.howe@hhu.de]
- Merge branch 'v1.0.53' into 'main' [qtbadmin qtbadmin]

  1.0.53

  See merge request qtb-hhu/dismo!54
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.53 (2024-11-24)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.52' into 'main' [qtbadmin qtbadmin]

  1.0.52

  See merge request qtb-hhu/dismo!53
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.52 (2024-11-17)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.51' into 'main' [qtbadmin qtbadmin]

  1.0.51

  See merge request qtb-hhu/dismo!52
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.51 (2024-11-10)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.50' into 'main' [qtbadmin qtbadmin]

  1.0.50

  See merge request qtb-hhu/dismo!51
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.50 (2024-11-03)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Bot: chg: updated pre-commit. [daniel.howe@hhu.de]
- Merge branch 'v1.0.49' into 'main' [qtbadmin qtbadmin]

  1.0.49

  See merge request qtb-hhu/dismo!50
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.49 (2024-10-27)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.48' into 'main' [qtbadmin qtbadmin]

  1.0.48

  See merge request qtb-hhu/dismo!49
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.48 (2024-10-20)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.47' into 'main' [qtbadmin qtbadmin]

  1.0.47

  See merge request qtb-hhu/dismo!48
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.47 (2024-10-13)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Bot: chg: updated pre-commit. [daniel.howe@hhu.de]
- Merge branch 'v1.0.46' into 'main' [qtbadmin qtbadmin]

  1.0.46

  See merge request qtb-hhu/dismo!47
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.46 (2024-09-29)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Bot: chg: updated pre-commit. [daniel.howe@hhu.de]
- Merge branch 'v1.0.45' into 'main' [qtbadmin qtbadmin]

  1.0.45

  See merge request qtb-hhu/dismo!46
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.45 (2024-09-22)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.44' into 'main' [qtbadmin qtbadmin]

  1.0.44

  See merge request qtb-hhu/dismo!45
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.44 (2024-09-15)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.43' into 'main' [qtbadmin qtbadmin]

  1.0.43

  See merge request qtb-hhu/dismo!44
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.43 (2024-09-08)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.42' into 'main' [qtbadmin qtbadmin]

  1.0.42

  See merge request qtb-hhu/dismo!43
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.42 (2024-09-01)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.41' into 'main' [qtbadmin qtbadmin]

  1.0.41

  See merge request qtb-hhu/dismo!42
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.41 (2024-08-25)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.40' into 'main' [qtbadmin qtbadmin]

  1.0.40

  See merge request qtb-hhu/dismo!41
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.40 (2024-08-18)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.39' into 'main' [qtbadmin qtbadmin]

  1.0.39

  See merge request qtb-hhu/dismo!40
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.39 (2024-08-11)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.38' into 'main' [qtbadmin qtbadmin]

  1.0.38

  See merge request qtb-hhu/dismo!39
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.38 (2024-08-04)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Bot: chg: updated pre-commit. [daniel.howe@hhu.de]
- Merge branch 'v1.0.37' into 'main' [qtbadmin qtbadmin]

  1.0.37

  See merge request qtb-hhu/dismo!38
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.37 (2024-07-28)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.36' into 'main' [qtbadmin qtbadmin]

  1.0.36

  See merge request qtb-hhu/dismo!37
- Bot: chg: updated changelog. [daniel.howe@hhu.de]


1.0.36 (2024-07-25)
-------------------
- Bot: chg: updated dependencies. [daniel.howe@hhu.de]
- Merge branch 'v1.0.35' into 'main' [qtbadmin qtbadmin]

  1.0.35

  See merge request qtb-hhu/dismo!36
- Bot: chg: updated changelog. [Dan Howe]


1.0.35 (2024-07-17)
-------------------
- Bot: chg: updated dependencies. [Dan Howe]
- Merge branch 'v1.0.34' into 'main' [Marvin van Aalst]

  1.0.34

  See merge request qtb-hhu/dismo!35
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.34 (2024-07-01)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.33' into 'main' [Marvin van Aalst]

  1.0.33

  See merge request qtb-hhu/dismo!34
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.33 (2024-06-24)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.32' into 'main' [Marvin van Aalst]

  1.0.32

  See merge request qtb-hhu/dismo!33
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.32 (2024-06-17)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.31' into 'main' [Marvin van Aalst]

  1.0.31

  See merge request qtb-hhu/dismo!32
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.31 (2024-06-10)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.30' into 'main' [Marvin van Aalst]

  1.0.30

  See merge request qtb-hhu/dismo!31
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.30 (2024-06-03)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.29' into 'main' [Marvin van Aalst]

  1.0.29

  See merge request qtb-hhu/dismo!30
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.29 (2024-05-27)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.28' into 'main' [Marvin van Aalst]

  1.0.28

  See merge request qtb-hhu/dismo!29
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.28 (2024-05-20)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.27' into 'main' [Marvin van Aalst]

  1.0.27

  See merge request qtb-hhu/dismo!28
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.27 (2024-05-13)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.26' into 'main' [Marvin van Aalst]

  1.0.26

  See merge request qtb-hhu/dismo!27
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.26 (2024-05-06)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.25' into 'main' [Marvin van Aalst]

  1.0.25

  See merge request qtb-hhu/dismo!26
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.25 (2024-05-03)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.24' into 'main' [Marvin van Aalst]

  1.0.24

  See merge request qtb-hhu/dismo!25
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.24 (2024-04-15)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.23' into 'main' [Marvin van Aalst]

  1.0.23

  See merge request qtb-hhu/dismo!24
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.23 (2024-04-08)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.22' into 'main' [Marvin van Aalst]

  1.0.22

  See merge request qtb-hhu/dismo!23
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.22 (2024-04-01)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.21' into 'main' [Marvin van Aalst]

  1.0.21

  See merge request qtb-hhu/dismo!22
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.21 (2024-03-25)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.20' into 'main' [Marvin van Aalst]

  1.0.20

  See merge request qtb-hhu/dismo!21
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.20 (2024-03-18)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.19' into 'main' [Marvin van Aalst]

  1.0.19

  See merge request qtb-hhu/dismo!20
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.19 (2024-03-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.18' into 'main' [Marvin van Aalst]

  1.0.18

  See merge request qtb-hhu/dismo!19
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.18 (2024-02-26)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.17' into 'main' [Marvin van Aalst]

  1.0.17

  See merge request qtb-hhu/dismo!18
- Bot: chg: updated changelog. [Marvin van Aalst]
- Bot: chg: updated poetry lockfile. [Marvin van Aalst]


1.0.17 (2024-02-21)
-------------------
- Dev: new: ruff instead of black / flake8. [Marvin van Aalst]
- Doc: new: multi-city sir model. [Marvin van Aalst]
- Dev: new: ruff instead of black / flake8. [Marvin van Aalst]
- Dev: fix: use assimulo for docs. [Marvin van Aalst]
- Merge branch 'v1.0.16' into 'main' [Marvin van Aalst]

  1.0.16

  See merge request qtb-hhu/dismo!17
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.16 (2024-02-20)
-------------------
- Dev: chg: increased version number. [Marvin van Aalst]
- Dev: fix: use both import and module not found error for assimulo.
  [Marvin van Aalst]
- Dev: fix: added dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.15' into 'main' [Marvin van Aalst]

  1.0.15

  See merge request qtb-hhu/dismo!16
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.15 (2024-02-20)
-------------------
- Dev: fix: added dependencies. [Marvin van Aalst]
- Dev: fix: docs environment. [Marvin van Aalst]
- Doc: chg: illustration of complex shapes at the beginning. [Marvin van
  Aalst]
- Dev: fix: don't supply hashes for docs requirements. [Marvin van
  Aalst]
- Dev: fix: requirements of docs. [Marvin van Aalst]
- Dev: fix: docs. [Marvin van Aalst]
- Dev: chg: run notebooks with mkdocs. [Marvin van Aalst]
- Merge branch 'v1.0.14' into 'main' [Marvin van Aalst]

  1.0.14

  See merge request qtb-hhu/dismo!15
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.14 (2024-02-19)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.13' into 'main' [Marvin van Aalst]

  1.0.13

  See merge request qtb-hhu/dismo!14
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.13 (2024-02-12)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.12' into 'main' [Marvin van Aalst]

  1.0.12

  See merge request qtb-hhu/dismo!13
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.12 (2024-02-05)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.11' into 'main' [Marvin van Aalst]

  1.0.11

  See merge request qtb-hhu/dismo!12
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.11 (2024-01-29)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.10' into 'main' [Marvin van Aalst]

  1.0.10

  See merge request qtb-hhu/dismo!11
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.10 (2024-01-15)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.9' into 'main' [Marvin van Aalst]

  1.0.9

  See merge request qtb-hhu/dismo!10
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.9 (2024-01-08)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.8' into 'main' [Marvin van Aalst]

  1.0.8

  See merge request qtb-hhu/dismo!9
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.8 (2024-01-01)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.7' into 'main' [Marvin van Aalst]

  1.0.7

  See merge request qtb-hhu/dismo!8
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.7 (2023-12-25)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.6' into 'main' [Marvin van Aalst]

  1.0.6

  See merge request qtb-hhu/dismo!7
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.6 (2023-12-20)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.5' into 'main' [Marvin van Aalst]

  1.0.5

  See merge request qtb-hhu/dismo!6
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.5 (2023-12-11)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.4' into 'main' [Marvin van Aalst]

  1.0.4

  See merge request qtb-hhu/dismo!5
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.4 (2023-11-13)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: pre-commit. [Marvin van Aalst]
- Dev: chg: pre-commit. [Marvin van Aalst]
- Merge branch 'v1.0.3' into 'main' [Marvin van Aalst]

  1.0.3

  See merge request qtb-hhu/dismo!4
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.3 (2023-11-09)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Dev: new: included security checks with bandit. [Marvin van Aalst]
- Merge branch 'v1.0.2' into 'main' [Marvin van Aalst]

  1.0.2

  See merge request qtb-hhu/dismo!3
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.2 (2023-11-06)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v1.0.1' into 'main' [Marvin van Aalst]

  1.0.1

  See merge request qtb-hhu/dismo!2
- Bot: chg: updated changelog. [Marvin van Aalst]


1.0.1 (2023-10-30)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: new: documentation. [Marvin van Aalst]
- Dev: new: documentation. [Marvin van Aalst]
- Dev: chg: updated readme. [Marvin van Aalst]
- Dev: chg: only run build when pyproject.toml was changed. [Marvin van
  Aalst]


1.0.0 (2023-10-18)
------------------
- Merge branch 'v1.0.0' into 'main' [Marvin van Aalst]

  v1.0.0

  See merge request qtb-hhu/dismo!1
- Dev: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: source activate in ci. [Marvin van Aalst]
- Dev: chg: source in ci. [Marvin van Aalst]
- Dev: chg: mamba init in ci. [Marvin van Aalst]
- Dev: chg: no need for assimulo in build stage. [Marvin van Aalst]
- Dev: chg: new env in tests. [Marvin van Aalst]
- Dev: chg: poetry lock. [Marvin van Aalst]
- Dev: chg: lowered minimal python version. [Marvin van Aalst]
- Dev: chg: tests and preparation for version 1.0 release. [Marvin van
  Aalst]
- Dev: chg: updated docs. [Marvin van Aalst]
- Dev: chg: backup. [Marvin van Aalst]
- Dev: new: more leaf examples. [Marvin van Aalst]
- Dev: fix: types§ [Marvin van Aalst]
- Dev: chg: renamed modelbase_pde to dismo. [Marvin van Aalst]
- Dev: chg: updated docs. [Marvin van Aalst]
- Dev: chg: unified naming. [Marvin van Aalst]
- Dev: fix: tests. [Marvin van Aalst]
- Dev: chg: hard-coded diffusion processes. [Marvin van Aalst]
- Dev: fix: tests. [Marvin van Aalst]
- Dev: chg: minor updates. [Marvin van Aalst]
- Dev: chg: more documentation. [Marvin van Aalst]
- Dev: new: example for vein and stoma model. [Marvin van Aalst]
- Dev: chg: moved leaf model description to improved interface. [Marvin
  van Aalst]
- Dev: new: added experimental quiver methods. [Marvin van Aalst]
- Dev: new: t_eval argument for simulate. [Marvin van Aalst]
- Dev: chg: use pyo3 rust-based diffusion functions in separate package
  instead. [Marvin van Aalst]
- Dev: fix: plotting of 1D structures. [Marvin van Aalst]
- Dev: chg: first consistently typed version. [Marvin van Aalst]


0.2.44 (2023-10-02)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.43' into 'main' [Marvin van Aalst]

  0.2.43

  See merge request qtb-hhu/modelbase-pde!43
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.43 (2023-09-25)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.42' into 'main' [Marvin van Aalst]

  0.2.42

  See merge request qtb-hhu/modelbase-pde!42
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.42 (2023-09-18)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.41' into 'main' [Marvin van Aalst]

  0.2.41

  See merge request qtb-hhu/modelbase-pde!41
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.41 (2023-09-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.40' into 'main' [Marvin van Aalst]

  0.2.40

  See merge request qtb-hhu/modelbase-pde!40
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.40 (2023-09-04)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.39' into 'main' [Marvin van Aalst]

  0.2.39

  See merge request qtb-hhu/modelbase-pde!39
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.39 (2023-08-28)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.38' into 'main' [Marvin van Aalst]

  0.2.38

  See merge request qtb-hhu/modelbase-pde!38
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.38 (2023-08-21)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.37' into 'main' [Marvin van Aalst]

  0.2.37

  See merge request qtb-hhu/modelbase-pde!37
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.37 (2023-08-14)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.36' into 'main' [Marvin van Aalst]

  0.2.36

  See merge request qtb-hhu/modelbase-pde!36
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.36 (2023-08-07)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.35' into 'main' [Marvin van Aalst]

  0.2.35

  See merge request qtb-hhu/modelbase-pde!35
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.35 (2023-07-31)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.34' into 'main' [Marvin van Aalst]

  0.2.34

  See merge request qtb-hhu/modelbase-pde!34
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.34 (2023-07-24)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.33' into 'main' [Marvin van Aalst]

  0.2.33

  See merge request qtb-hhu/modelbase-pde!33
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.33 (2023-07-17)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.32' into 'main' [Marvin van Aalst]

  0.2.32

  See merge request qtb-hhu/modelbase-pde!32
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.32 (2023-07-10)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.31' into 'main' [Marvin van Aalst]

  0.2.31

  See merge request qtb-hhu/modelbase-pde!31
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.31 (2023-07-03)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.30' into 'main' [Marvin van Aalst]

  0.2.30

  See merge request qtb-hhu/modelbase-pde!30
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.30 (2023-06-26)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.29' into 'main' [Marvin van Aalst]

  0.2.29

  See merge request qtb-hhu/modelbase-pde!29
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.29 (2023-06-19)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.28' into 'main' [Marvin van Aalst]

  0.2.28

  See merge request qtb-hhu/modelbase-pde!28
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.28 (2023-06-12)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.27' into 'main' [Marvin van Aalst]

  0.2.27

  See merge request qtb-hhu/modelbase-pde!27
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.27 (2023-06-05)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.26' into 'main' [Marvin van Aalst]

  0.2.26

  See merge request qtb-hhu/modelbase-pde!26
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.26 (2023-05-30)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.25' into 'main' [Marvin van Aalst]

  0.2.25

  See merge request qtb-hhu/modelbase-pde!25
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.25 (2023-05-15)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.24' into 'main' [Marvin van Aalst]

  0.2.24

  See merge request qtb-hhu/modelbase-pde!24
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.24 (2023-05-08)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.23' into 'main' [Marvin van Aalst]

  0.2.23

  See merge request qtb-hhu/modelbase-pde!23
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.23 (2023-05-01)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.22' into 'main' [Marvin van Aalst]

  0.2.22

  See merge request qtb-hhu/modelbase-pde!22
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.22 (2023-04-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.21' into 'main' [Marvin van Aalst]

  0.2.21

  See merge request qtb-hhu/modelbase-pde!21
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.21 (2023-03-31)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.20' into 'main' [Marvin van Aalst]

  0.2.20

  See merge request qtb-hhu/modelbase-pde!20
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.20 (2023-03-14)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.19' into 'main' [Marvin van Aalst]

  0.2.19

  See merge request qtb-hhu/modelbase-pde!19
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.19 (2023-03-13)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.18' into 'main' [Marvin van Aalst]

  0.2.18

  See merge request qtb-hhu/modelbase-pde!18
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.18 (2023-03-06)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.17' into 'main' [Marvin van Aalst]

  0.2.17

  See merge request qtb-hhu/modelbase-pde!17
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.17 (2023-02-21)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.16' into 'main' [Marvin van Aalst]

  0.2.16

  See merge request qtb-hhu/modelbase-pde!16
- Dev: chg: removed unused import. [Marvin van Aalst]
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.16 (2023-02-06)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.15' into 'main' [Marvin van Aalst]

  0.2.15

  See merge request qtb-hhu/modelbase-pde!15
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.15 (2023-01-30)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.14' into 'main' [Marvin van Aalst]

  0.2.14

  See merge request qtb-hhu/modelbase-pde!14
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.14 (2023-01-23)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.13' into 'main' [Marvin van Aalst]

  0.2.13

  See merge request qtb-hhu/modelbase-pde!13
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.13 (2023-01-16)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.12' into 'main' [Marvin van Aalst]

  0.2.12

  See merge request qtb-hhu/modelbase-pde!12
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.12 (2023-01-09)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.11' into 'main' [Marvin van Aalst]

  0.2.11

  See merge request qtb-hhu/modelbase-pde!11
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.11 (2023-01-09)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.10' into 'main' [Marvin van Aalst]

  0.2.10

  See merge request qtb-hhu/modelbase-pde!10
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.10 (2023-01-02)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.9' into 'main' [Marvin van Aalst]

  0.2.9

  See merge request qtb-hhu/modelbase-pde!9
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.9 (2022-12-27)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.8' into 'main' [Marvin van Aalst]

  0.2.8

  See merge request qtb-hhu/modelbase-pde!8
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.8 (2022-12-12)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.7' into 'main' [Marvin van Aalst]

  0.2.7

  See merge request qtb-hhu/modelbase-pde!7
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.7 (2022-11-29)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.6

  See merge request qtb-hhu/modelbase-pde!6
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.6 (2022-11-21)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: updated python version bound. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.5

  See merge request qtb-hhu/modelbase-pde!5
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.5 (2022-11-21)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.4

  See merge request qtb-hhu/modelbase-pde!4
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.4 (2022-11-14)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.3

  See merge request qtb-hhu/modelbase-pde!3
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.3 (2022-11-07)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  Dependencies

  See merge request qtb-hhu/modelbase-pde!2
- Bot: chg: updated changelog. [Marvin van Aalst]
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: updated ci container. [Marvin van Aalst]
- Bot: chg: updated changelog. [Marvin van Aalst]
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: old. [Marvin van Aalst]
- Dev: chg: added badges to readme. [Marvin van Aalst]
- Dev: chg: fixed poetry build. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.1.1

  See merge request qtb-hhu/modelbase-pde!1
- Bot: chg: updated changelog. [Marvin van Aalst]


0.1.1 (2022-09-27)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: added changelog to dev dependencies. [Marvin van Aalst]


0.1.0 (2022-09-26)
------------------
- Dev: chg: changed to poetry build. [Marvin van Aalst]
- Dev: chg: proper layout. [Marvin van Aalst]
- Dev: new: added .gitattributes. [Marvin van Aalst]
- Updated version number. [Marvin van Aalst]
- Added some tests. [Marvin van Aalst]
- Fixed setup.py. [Marvin van Aalst]
- Added import tests. [Marvin van Aalst]
- Added modelbase dependency. [Marvin van Aalst]
- Added test backbone. [Marvin van Aalst]
- Initial commit. [Marvin van Aalst]


