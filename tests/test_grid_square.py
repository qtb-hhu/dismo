from __future__ import annotations

import matplotlib.pyplot as plt
import numpy as np

from dismo.grids import SquareGrid


def test_get_nearest_neighbor_of_celltype() -> None:
    grid = SquareGrid((3, 3))
    grid.add_celltype("a", 1)
    grid.add_celltype("b", 2)
    grid.add_cell((1, 1), "a")
    grid.add_cell((2, 2), "b")
    assert grid.nearest_neighbor_of_celltype((1, 1), "b") == 2


def test_get_nearest_distances_of_celltypes() -> None:
    grid = SquareGrid((3, 3))
    grid.add_celltype("a", 1)
    grid.add_celltype("b", 2)
    grid.add_cell((1, 1), "a")
    grid.add_cell((2, 2), "b")
    assert grid.nearest_neighbor_of_celltype((1, 1), "b") == 2
    assert grid.nearest_distances_of_celltypes("a", "b") == [2]


def test_get_cell_neighbors_2d() -> None:
    grid = SquareGrid((3, 3))
    grid.add_celltype("a", 1)
    grid.add_cell((1, 1), "a")
    assert np.array_equal(grid.get_cell_neighbors(0), [-1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(1), [-1, -1, 4, -1])
    assert np.array_equal(grid.get_cell_neighbors(2), [-1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(3), [-1, 4, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(4), [-1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(5), [-1, -1, -1, 4])
    assert np.array_equal(grid.get_cell_neighbors(6), [-1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(7), [4, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(8), [-1, -1, -1, -1])


def test_initialize_2d() -> None:
    m = SquareGrid((3, 3))
    m.add_celltype("a", 1)
    m.add_cell((1, 1), "a")
    m.add_cell((0, 1), "a")
    m.add_cell((2, 1), "a")
    m.add_cell((1, 2), "a")
    m.add_cell((1, 0), "a")
    m.initialize()
    assert np.array_equal(
        m.neighbors[("a", "a")],
        [
            [-1, -1, -1, -1],
            [-1, -1, 4, -1],
            [-1, -1, -1, -1],
            [-1, 4, -1, -1],
            [1, 5, 7, 3],
            [-1, -1, -1, 4],
            [-1, -1, -1, -1],
            [4, -1, -1, -1],
            [-1, -1, -1, -1],
        ],
    )


def test_plot_axes() -> None:
    grid = SquareGrid((3, 3))
    grid.plot_axes()
    plt.close()


def test_plot_axes_existing() -> None:
    grid = SquareGrid((3, 3))
    fig, ax = plt.subplots()
    grid.plot_axes(ax=ax)
    plt.close()


def test_plot_grid() -> None:
    grid = SquareGrid((3, 3))
    fig, ax = plt.subplots()
    grid.plot_grid(ax=ax)
    plt.close()


def test_plot_cell_coordinates() -> None:
    grid = SquareGrid((3, 3))
    fig, ax = plt.subplots()
    grid.plot_cell_coordinates(ax=ax)
    plt.close()
