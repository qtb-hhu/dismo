from __future__ import annotations

import matplotlib.pyplot as plt
import numpy as np
import pytest

from dismo.grids import CubeGrid, RodGrid, SquareGrid
from dismo.models import StomataModel


def make_rod_model() -> StomataModel:
    model = StomataModel(
        grid=RodGrid((5,)),
        ps_k=0,
        ps_capacity=1,
        suc_meso_to_meso=0.5,
        vein_base_coordinates=(1,),
        suc_meso_to_vein=1.0,
        suc_vein_to_vein=1.0,
        co2_k=1.0,
        co2_capacity=1.0,
        co2_stoma_to_meso=1.0,
        co2_meso_to_meso=1.0,
    )
    for i in range(1):
        model.add_mesophyll_cell((i,))
    for i in range(1, 3):
        model.add_vein_cell((i,))
    for i in range(3, 5):
        model.add_stoma_cell((i,))
    return model


def make_square_model() -> StomataModel:
    model = StomataModel(
        grid=SquareGrid((3, 3)),
        ps_k=0,
        ps_capacity=1,
        suc_meso_to_meso=0.5,
        vein_base_coordinates=(0, 0),
        suc_meso_to_vein=1.0,
        suc_vein_to_vein=1.0,
        co2_k=1.0,
        co2_capacity=1.0,
        co2_stoma_to_meso=1.0,
        co2_meso_to_meso=1.0,
    )
    model.add_mesophyll_cell((1, 1))
    model.add_vein_cell((0, 1))
    model.add_stoma_cell((2, 1))
    model.add_mesophyll_cell((1, 2))
    model.add_mesophyll_cell((1, 0))
    return model


def make_cube_model() -> StomataModel:
    model = StomataModel(
        grid=CubeGrid((3, 3, 3)),
        ps_k=0,
        ps_capacity=1,
        suc_meso_to_meso=0.5,
        vein_base_coordinates=(0, 0, 0),
        suc_meso_to_vein=1.0,
        suc_vein_to_vein=1.0,
        co2_k=1.0,
        co2_capacity=1.0,
        co2_stoma_to_meso=1.0,
        co2_meso_to_meso=1.0,
    )
    model.add_mesophyll_cell((1, 1, 1))
    model.add_mesophyll_cell((2, 1, 1))
    model.add_vein_cell((0, 1, 1))
    model.add_mesophyll_cell((1, 2, 1))
    model.add_stoma_cell((1, 0, 1))
    model.add_mesophyll_cell((1, 1, 0))
    model.add_mesophyll_cell((1, 1, 2))
    return model


def test_get_influxes() -> None:
    model = make_rod_model()
    influxes = model.get_influxes(
        np.hstack(
            (
                np.array([0, 0, 0, 0, 0], dtype=float),
                np.array([0, 0, 0, 0, 0], dtype=float),
            )
        )
    )

    assert np.array_equal(influxes["sucrose"], np.array([0, 0, 0, 0, 0], dtype=float))


def test_get_influxes_by_process() -> None:
    model = make_rod_model()
    influxes = model.get_influxes_by_process(
        np.hstack(
            (
                np.array([0, 0, 0, 0, 0], dtype=float),
                np.array([0, 0, 0, 0, 0], dtype=float),
            )
        )
    )

    assert np.array_equal(
        influxes["sucrose"]["photosynthesis"], np.array([0], dtype=float)
    )
    assert np.array_equal(influxes["co2"]["stomata"], np.array([1.0, 1.0], dtype=float))


def test_get_diffusion() -> None:
    model = make_rod_model()
    diffusion = model.get_diffusion(
        np.hstack(
            (
                np.array([0, 0, 0, 0, 0], dtype=float),
                np.array([0, 0, 0, 0, 0], dtype=float),
            )
        )
    )

    assert np.array_equal(diffusion["sucrose"], np.array([0, 0, 0, 0, 0], dtype=float))
    assert np.array_equal(diffusion["co2"], np.array([0, 0, 0, 0, 0], dtype=float))


def test_get_diffusion_by_process() -> None:
    model = make_rod_model()
    diffusion = model.get_diffusion_by_process(
        np.hstack(
            (
                np.array([0, 0, 0, 0, 0], dtype=float),
                np.array([0, 0, 0, 0, 0], dtype=float),
            )
        )
    )

    assert np.array_equal(
        diffusion["sucrose"]["meso_to_meso"], np.array([0, 0, 0, 0.0, 0], dtype=float)
    )


def test_right_hand_side_1d() -> None:
    model = make_rod_model()
    y0 = np.zeros(5 * 2, dtype=float)
    y0[2] = 1
    assert np.array_equal(
        model.get_right_hand_side(y0),
        [0.0, 1.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0],
    )


def test_right_hand_side_2d() -> None:
    model = make_square_model()
    y0 = np.zeros(3 * 3 * 2, dtype=float)
    y0[4] = 1
    assert np.array_equal(
        model.get_right_hand_side(y0),
        [
            0.0,
            1.0,
            0.0,
            0.5,
            -2.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            0.0,
        ],
    )


def test_right_hand_side_3d() -> None:
    model = make_cube_model()
    y0 = np.zeros(3 * 3 * 3 * 2, dtype=float)
    y0[13] = 1
    assert np.array_equal(
        model.get_right_hand_side(y0),
        [
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.5,
            -3.0,
            0.5,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
        ],
    )


def test_plot() -> None:
    model = make_rod_model()
    model.plot()
    plt.close()

    model.plot(concentrations=np.ones(5))
    plt.close()

    model.plot(concentrations=np.ones(5), alpha=0.5)
    plt.close()

    model.plot(concentrations=np.ones(5), annotate=True)
    plt.close()

    with pytest.raises(ValueError):
        model.plot(concentrations=None, annotate=True)
