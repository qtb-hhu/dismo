import matplotlib.pyplot as plt

from dismo.coordinates import Axial


def test_neighbors() -> None:
    assert Axial(1, 1).neighbors() == [
        Axial(2, 1),
        Axial(2, 0),
        Axial(1, 0),
        Axial(0, 1),
        Axial(0, 2),
        Axial(1, 2),
    ]


def test_distance() -> None:
    assert Axial(1, 1).distance(Axial(0, 0)) == 2
    assert Axial(1, 1).distance(Axial(2, 0)) == 1


def test_to_cube() -> None:
    from dismo.coordinates import Cube

    assert Axial(1, 1).to_cube() == Cube(1, -2, 1)


def test_to_oddq() -> None:
    from dismo.coordinates import Oddq

    assert Axial(1, 1).to_oddq() == Oddq(1, 1)


def test_to_plot_coordinates() -> None:
    assert Axial(1, 1).to_plot_coordinates() == (1, 1)


def test_plot() -> None:
    fig, ax = plt.subplots()
    Axial(1, 1).plot(ax=ax, facecolor="black", edgecolor="black")
    assert True
    plt.close()
