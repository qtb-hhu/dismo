import matplotlib.pyplot as plt

from dismo.coordinates import Cube3D


def test_neighbors() -> None:
    assert Cube3D(1, 1, 1).neighbors() == [
        Cube3D(0, 1, 1),
        Cube3D(1, 2, 1),
        Cube3D(2, 1, 1),
        Cube3D(1, 0, 1),
        Cube3D(1, 1, 2),
        Cube3D(1, 1, 0),
    ]


def test_distance() -> None:
    assert Cube3D(1, 1, 1).distance(Cube3D(0, 0, 0)) == 1.0
    assert Cube3D(1, 1, 1).distance(Cube3D(2, 0, 0)) == 1.0


def test_to_plot_coordinates() -> None:
    assert Cube3D(1, 1, 1).to_plot_coordinates() == (1.5, 1.5, 1.5)


def test_plot() -> None:
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    Cube3D(1, 1, 1).plot(ax=ax, facecolor="black", edgecolor="black")
    assert True
    plt.close()
