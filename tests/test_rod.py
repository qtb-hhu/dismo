import matplotlib.pyplot as plt

from dismo.coordinates import Rod


def test_neighbors() -> None:
    assert Rod(1).neighbors() == [Rod(0), Rod(2)]


def test_to_plot_coordinates() -> None:
    assert Rod(1).to_plot_coordinates() == (1, 0)


def test_distance() -> None:
    assert Rod(1).distance(Rod(0)) == 1
    assert Rod(1).distance(Rod(2)) == 1


def test_plot() -> None:
    fig, ax = plt.subplots()
    Rod(1).plot(ax=ax, facecolor="black", edgecolor="black")
    assert True
    plt.close()
