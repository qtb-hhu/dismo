import matplotlib.pyplot as plt

from dismo.coordinates import Oddq


def test_neighbors() -> None:
    assert Oddq(1, 1).neighbors() == [
        Oddq(2, 2),
        Oddq(2, 1),
        Oddq(1, 0),
        Oddq(0, 1),
        Oddq(0, 2),
        Oddq(1, 2),
    ]


def test_to_plot_coordinates() -> None:
    assert Oddq(1, 1).to_plot_coordinates() == (1.5, 2.598076211353316)


def test_distance() -> None:
    assert Oddq(1, 1).distance(Oddq(0, 0)) == 2
    assert Oddq(1, 1).distance(Oddq(2, 0)) == 2


def test_to_cube() -> None:
    from dismo.coordinates import Cube

    assert Oddq(1, 1).to_cube() == Cube(1, -2, 1)


def test_to_axial() -> None:
    from dismo.coordinates import Axial

    assert Oddq(1, 1).to_axial() == Axial(1, 1)


def test_to_doubleheight() -> None:
    from dismo.coordinates import Doubleheight

    assert Oddq(1, 1).to_doubleheight() == Doubleheight(1, 3)


def test_plot() -> None:
    fig, ax = plt.subplots()
    Oddq(1, 1).plot(ax=ax, facecolor="black", edgecolor="black")
    assert True
    plt.close()
