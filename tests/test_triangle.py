import matplotlib.pyplot as plt

from dismo.coordinates import Triangle


def test_neighbors() -> None:
    assert Triangle(1, 0).neighbors() == [Triangle(1, 1), Triangle(2, 0), Triangle(0, 0)]
    assert Triangle(1, 1).neighbors() == [Triangle(2, 1), Triangle(1, 0), Triangle(0, 1)]


def test_to_plot_coordinates() -> None:
    assert Triangle(0, 0).to_plot_coordinates() == (0, 0)


def test_distance() -> None:
    assert Triangle(1, 1).distance(Triangle(0, 0)) == 2.0
    assert Triangle(1, 1).distance(Triangle(2, 0)) == 2.0


def test_plot() -> None:
    fig, ax = plt.subplots()
    Triangle(1, 1).plot(ax=ax, facecolor="black", edgecolor="black")
    assert True
    plt.close()
