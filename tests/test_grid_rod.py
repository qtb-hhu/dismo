from __future__ import annotations

import matplotlib.pyplot as plt
import numpy as np

from dismo.grids import RodGrid


def test_initialize_cells() -> None:
    grid = RodGrid((10,))
    assert np.array_equal(grid.cells, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0])


def test_add_celltype() -> None:
    grid = RodGrid((10,))
    grid.add_celltype("a", 1)
    assert grid.celltypes == {"a": 1}


def test_add_cell() -> None:
    grid = RodGrid((5,))
    grid.add_celltype("a", 1)
    grid.add_cell((2,), "a")
    assert np.array_equal(grid.cells, [0, 0, 1, 0, 0])


def test_add_variable() -> None:
    grid = RodGrid((5,))
    grid.add_celltype("a", 1)
    grid.add_variable("x")
    assert np.array_equal(grid.variables, ["x"])


def test_copy() -> None:
    grid = RodGrid((5,))
    assert grid.copy() is not grid


def test_generate_initial_values() -> None:
    grid = RodGrid((5,))
    grid.add_variable("a")
    assert (grid.generate_initial_values() == np.zeros((5, 1))).all()


def test_get_celltypes() -> None:
    grid = RodGrid((5,))
    grid.add_celltype("a", 1)
    assert grid.get_celltypes() == {"a": 1}


def test_get_celltype_of_idx() -> None:
    grid = RodGrid((5,))
    grid.add_celltype("a", 1)
    grid.add_cell((1,), "a")
    assert grid.get_celltype_of_index(0) == 0
    assert grid.get_celltype_of_index(1) == 1
    assert grid.get_celltype_of_index(2) == 0
    assert grid.get_celltype_of_index(3) == 0
    assert grid.get_celltype_of_index(4) == 0


def test_get_nearest_neighbor_of_celltype() -> None:
    grid = RodGrid((5,))
    grid.add_celltype("a", 1)
    grid.add_celltype("b", 2)
    grid.add_cell((1,), "a")
    grid.add_cell((2,), "b")
    assert grid.nearest_neighbor_of_celltype((1,), "b") == 1


def test_get_nearest_distances_of_celltypes() -> None:
    grid = RodGrid((5,))
    grid.add_celltype("a", 1)
    grid.add_celltype("b", 2)
    grid.add_cell((1,), "a")
    grid.add_cell((2,), "b")
    assert grid.nearest_distances_of_celltypes("a", "b") == [1]


def test_get_cell_neighbors_1d() -> None:
    grid = RodGrid((5,))
    grid.add_celltype("a", 1)
    grid.add_cell(((2,)), "a")
    assert np.array_equal(grid.get_cell_neighbors(0), [-1, -1])
    assert np.array_equal(grid.get_cell_neighbors(1), [-1, 2])
    assert np.array_equal(grid.get_cell_neighbors(2), [-1, -1])
    assert np.array_equal(grid.get_cell_neighbors(3), [2, -1])
    assert np.array_equal(grid.get_cell_neighbors(4), [-1, -1])


def test_initialize_1d() -> None:
    grid = RodGrid((5,))
    grid.add_celltype("a", 1)
    grid.add_cell((1,), "a")
    grid.add_cell((2,), "a")
    grid.add_cell((3,), "a")
    grid.initialize()
    assert np.array_equal(
        grid.neighbors[("a", "a")],
        [[-1, -1], [-1, 2], [1, 3], [2, -1], [-1, -1]],
    )


def test_plot_axes() -> None:
    grid = RodGrid((5,))
    grid.plot_axes()
    plt.close()


def test_plot_axes_existing() -> None:
    grid = RodGrid((5,))
    fig, ax = plt.subplots()
    grid.plot_axes(ax=ax)
    plt.close()


def test_plot_grid() -> None:
    grid = RodGrid((5,))
    fig, ax = plt.subplots()
    grid.plot_grid(ax=ax)
    plt.close()


def test_plot_cell_coordinates() -> None:
    grid = RodGrid((5,))
    fig, ax = plt.subplots()
    grid.plot_cell_coordinates(ax=ax)
    plt.close()


def test_plot_cell_concentration() -> None:
    grid = RodGrid((5,))
    grid.add_celltype("a", 1)
    grid.add_cell((1,), "a")

    fig, ax = plt.subplots()
    grid.plot_cell_concentration(
        ax=ax,
        concentrations=np.array([1, 2, 3, 4, 5]),
    )
    plt.close()
