import matplotlib.pyplot as plt

from dismo.coordinates import Doubleheight


def test_neighbors() -> None:
    assert Doubleheight(1, 1).neighbors() == [
        Doubleheight(2, 2),
        Doubleheight(2, 0),
        Doubleheight(1, -1),
        Doubleheight(0, 0),
        Doubleheight(0, 2),
        Doubleheight(1, 3),
    ]


def test_distance() -> None:
    assert Doubleheight(1, 1).distance(Doubleheight(0, 0)) == 1
    assert Doubleheight(1, 1).distance(Doubleheight(2, 0)) == 1


def test_to_cube() -> None:
    from dismo.coordinates import Cube

    assert Doubleheight(1, 1).to_cube() == Cube(1, -1, 0)


def test_to_oddq() -> None:
    from dismo.coordinates import Oddq

    assert Doubleheight(1, 1).to_oddq() == Oddq(1, 0)


def test_to_plot_coordinates() -> None:
    assert Doubleheight(1, 1).to_plot_coordinates() == (1, 1)


def test_plot() -> None:
    fig, ax = plt.subplots()
    Doubleheight(1, 1).plot(ax=ax, facecolor="black", edgecolor="black")
    assert True
    plt.close()
