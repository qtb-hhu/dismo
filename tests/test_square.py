import matplotlib.pyplot as plt

from dismo.coordinates import Square


def test_neighbors() -> None:
    assert Square(1, 1).neighbors() == [
        Square(0, 1),
        Square(1, 2),
        Square(2, 1),
        Square(1, 0),
    ]


def test_to_plot_coordinates() -> None:
    assert Square(1, 1).to_plot_coordinates() == (1, 1)


def test_distance() -> None:
    assert Square(1, 1).distance(Square(0, 0)) == 2
    assert Square(1, 1).distance(Square(2, 0)) == 2


def test_to_oddq() -> None:
    from dismo.coordinates import Oddq

    assert Square(1, 1).to_oddq() == Oddq(0, 0)


def test_to_triangle() -> None:
    from dismo.coordinates import Triangle

    assert Square(1, 1).to_triangle() == Triangle(1, 1)


def test_plot() -> None:
    fig, ax = plt.subplots()
    Square(1, 1).plot(ax=ax, facecolor="black", edgecolor="black")
    assert True
    plt.close()
