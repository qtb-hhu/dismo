import matplotlib.pyplot as plt

from dismo.coordinates import Cube


def test_neighbors() -> None:
    assert Cube(1, 1, 1).neighbors() == [
        Cube(2, 0, 1),
        Cube(2, 1, 0),
        Cube(1, 2, 0),
        Cube(0, 2, 1),
        Cube(0, 1, 2),
        Cube(1, 0, 2),
    ]


def test_ring() -> None:
    assert Cube(1, 1, 1).ring(1) == [
        Cube(0, 1, 2),
        Cube(1, 0, 2),
        Cube(2, 0, 1),
        Cube(2, 1, 0),
        Cube(1, 2, 0),
        Cube(0, 2, 1),
    ]


def test_distance() -> None:
    assert Cube(1, 1, 1).distance(Cube(0, 0, 0)) == 1
    assert Cube(1, 1, 1).distance(Cube(2, 0, 0)) == 1


def test_to_doubleheight() -> None:
    from dismo.coordinates import Doubleheight

    assert Cube(1, 1, 1).to_doubleheight() == Doubleheight(1, 3)


def test_to_axial() -> None:
    from dismo.coordinates import Axial

    assert Cube(1, 1, 1).to_axial() == Axial(1, 1)


def test_to_oddq() -> None:
    from dismo.coordinates import Oddq

    assert Cube(1, 1, 1).to_oddq() == Oddq(1, 1)


def test_to_plot_coordinates() -> None:
    assert Cube(1, 1, 1).to_plot_coordinates() == (1, 1, 1)


def test_plot() -> None:
    fig, ax = plt.subplots()
    Cube(1, 1, 1).plot(ax=ax, facecolor="black", edgecolor="black")
    assert True
    plt.close()
