from __future__ import annotations

import matplotlib.pyplot as plt

from dismo.grids import TriGrid


def test_plot_axes() -> None:
    grid = TriGrid((3, 3))
    grid.plot_axes()
    plt.close()


def test_plot_axes_existing() -> None:
    grid = TriGrid((3, 3))
    fig, ax = plt.subplots()
    grid.plot_axes(ax=ax)
    plt.close()


def test_plot_grid() -> None:
    grid = TriGrid((3, 3))
    fig, ax = plt.subplots()
    grid.plot_grid(ax=ax)
    plt.close()


def test_plot_cell_coordinates() -> None:
    grid = TriGrid((3, 3))
    fig, ax = plt.subplots()
    grid.plot_cell_coordinates(ax=ax)
    plt.close()
