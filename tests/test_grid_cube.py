from __future__ import annotations

import matplotlib.pyplot as plt
import numpy as np

from dismo.grids import CubeGrid


def test_get_cell_neighbors_3d() -> None:
    grid = CubeGrid((3, 3, 3))
    grid.add_celltype("a", 1)
    grid.add_cell((1, 1, 1), "a")
    assert np.array_equal(grid.get_cell_neighbors(0), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(1), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(2), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(3), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(4), [-1, -1, 13, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(5), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(6), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(7), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(8), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(9), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(10), [-1, 13, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(11), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(12), [-1, -1, -1, -1, 13, -1])
    assert np.array_equal(grid.get_cell_neighbors(13), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(14), [-1, -1, -1, -1, -1, 13])
    assert np.array_equal(grid.get_cell_neighbors(15), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(16), [-1, -1, -1, 13, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(17), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(18), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(19), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(20), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(21), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(22), [13, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(23), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(24), [-1, -1, -1, -1, -1, -1])
    assert np.array_equal(grid.get_cell_neighbors(25), [-1, -1, -1, -1, -1, -1])


def test_initialize_3d() -> None:
    grid = CubeGrid((3, 3, 3))
    grid.add_celltype("a", 1)
    grid.add_cell((1, 1, 1), "a")
    grid.add_cell((2, 1, 1), "a")
    grid.add_cell((0, 1, 1), "a")
    grid.add_cell((1, 2, 1), "a")
    grid.add_cell((1, 0, 1), "a")
    grid.add_cell((1, 1, 0), "a")
    grid.add_cell((1, 1, 2), "a")
    grid.initialize()
    assert np.array_equal(
        grid.neighbors[("a", "a")],
        [
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, 13, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, 13, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, 13, -1],
            [4, 16, 22, 10, 14, 12],
            [-1, -1, -1, -1, -1, 13],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, 13, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [13, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1, -1],
        ],
    )


def test_plot_axes() -> None:
    grid = CubeGrid((3, 3, 3))
    grid.plot_axes()
    plt.close()


def test_plot_axes_existing() -> None:
    grid = CubeGrid((3, 3, 3))
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    grid.plot_axes(ax=ax)
    plt.close()


def test_plot_grid() -> None:
    grid = CubeGrid((3, 3, 3))
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    grid.plot_grid(ax=ax)
    plt.close()


def test_plot_cell_coordinates() -> None:
    grid = CubeGrid((3, 3, 3))
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    grid.plot_cell_coordinates(ax=ax)
    plt.close()
